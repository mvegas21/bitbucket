'use strict'
const gulp= required('gulp'),
      sass= required('gulp-sass');

gulp.task('sass', function(){
    gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'))
})